

class Location
{
    constructor() 
    {
        this._position = {
            x:0,
            y:0,
            z:0
        };
        this._size = {
            r: 1.0
        };
    }

    get position()
    {
        return this._position;
    }

    set position(value)
    {
        if("x" in value)
        {
            this._position.x = value.x;
        }
        if("y" in value)
        {
            this._position.y = value.y;
        }
        if("z" in value)
        {
            this._position.z = value.z;
        }
    }

    get radius()
    {
        return this._size.r;
    }

    set radius(value)
    {
        this._size.r = value;
    }

    move(value)
    {
        if("x" in value)
        {
            this._position.x += value.x;
        }
        if("y" in value)
        {
            this._position.y += value.y;
        }
        if("z" in value)
        {
            this._position.z += value.z;
        }
    }

    getTypeName()
    {
        return "Location";
    }
}

export { Location };
