
var staticID = 1000;
function createID()
{
    return staticID++;
}

class Entity
{
    constructor() 
    {
        this._id = createID();
        this._properties = {};
    }

    get id() 
    {
        return this._id;
    }

    has(type)
    {
        return (type in this._properties);
    }

    setProperty(type, value)
    {
        this._properties[type] = value;
    }

    getPropert(type)
    {
        if(type in this._properties)
        {
            return this._properties[type];
        }
        return null;
    }
}

export { Entity };
