
'use strict';
import {Platform} from "./platform.js";
import {Entity} from "./entity.js";
import {System} from "./system.js";
import {Loader} from "./loader.js";

// Bootstrap it
$(function(){
    var platform = new Platform();
    window.platform = platform;

    var system = new System();
    window.system = system;

    platform.log({
        message: "Hello"
    });

    $.getJSON("data.json", function( data ) {
        if("entity" in data)
        {
            var loader = new Loader(system);

            if(!loader.loadWorld(data.world))
            {
                platform.log({
                    message: "Failed to load World."
                });
            }
            platform.log({
                message: "World loaded."
            });
            platform.log({
                message: "call system.getWorld() to see what makes world."
            });
            if(!loader.loadEntities(data.entity))
            {
                platform.log({
                    message: "Failed to load entities."
                });
                return;
            }
            platform.log({
                message: "Entities loaded."
            });
            platform.log({
                message: "call system.getEntities() to see what entities are alive."
            });
        }
    });
});