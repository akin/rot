
import {Entity} from "./entity.js";
import {System} from "./system.js";
import {Resource} from "./resource.js";
import {Metadata} from "./metadata.js";
import {Location} from "./location.js";

function isset(item)
{
    return item != undefined;
}

class Loader
{
    constructor(system) 
    {
        this._system = system;
    }

    loadWorld(data)
    {
        // Load world..
        var world = this._system.getWorld();
        for(var type in data)
        {
            var arr = data[type];
            if(world.hasType(type))
            {
                for(var i = 0 ; i < arr.length ; ++i)
                {
                    world.add(type, arr[i]);
                }
            }
        }
        return true;
    }

    loadEntities(data)
    {
        // Load array of entities
        if(!(data instanceof Array))
        {
            return false;
        }

        for(var i = 0 ; i < data.length ; ++i)
        {
            var entity = this._system.createEntity();
            var meta = new Metadata();
            var item = data[i];
            entity.setProperty(meta.getTypeName(), meta);

            var location = new Location();
            entity.setProperty(location.getTypeName(), location);

            for(var key in item)
            {
                if(key == "resource")
                {
                    var resource = new Resource();
                    resource.add(item.resource);
                    entity.setProperty(resource.getTypeName(), resource);
                    continue;
                }
                if(key == "position")
                {
                    location.position = item.position;
                    continue;
                }
                if(key == "radius")
                {
                    location.radius = item.radius;
                    continue;
                }

                var foo = {};
                foo[key] = item[key];
                meta.set(foo);
            }
        }
        return true;
    }
}

export { Loader };
