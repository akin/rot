

class Metadata
{
    constructor() 
    {
        this._data = {};
    }

    set(item)
    {
        // Commit loop
        for(var key in item)
        {
            this._data[key] = item[key];
        }
    }

    get(key)
    {
        if(key in this._data)
        {
            return this._data[key];
        }
        return null;
    }

    getTypeName()
    {
        return "Metadata";
    }
}

export { Metadata };
