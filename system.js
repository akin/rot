'use strict';
import {Entity} from "./entity.js";
import {World} from "./world/world.js";

class System
{
    constructor() 
    {
        this._entities = {};
        this._world = new World();
    }

    createEntity()
    {
        var entity = new Entity();
        this._entities[entity.id] = entity;
        return entity;
    }

    hasEntity(id)
    {
        return id in this._entities;
    }

    getEntity(id)
    {
        if(id in this._entities)
        {
            return this._entities[id];
        }
        return null;
    }

    getEntities()
    {
        return this._entities;
    }

    getWorld()
    {
        return this._world;
    }
}

export { System };