

class Resource
{
    constructor() 
    {
        this._resource = {};
    }

    add(item)
    {
        // Check loop
        var fail = false;
        for(var key in item)
        {
            var value = item[key];
            if(key in this._resource)
            {
                value += this._resource[key];
            }

            if(value < 0)
            {
                return false;
            }
        }
        // Commit loop
        for(var key in item)
        {
            if(key in this._resource)
            {
                this._resource[key] += item[key];
            }
            else
            {
                this._resource[key] = item[key];
            }
        }
        return true;
    }

    get(key)
    {
        if(key in this._resource)
        {
            return this._resource[key];
        }
        return 0;
    }

    getTypeName()
    {
        return "Resource";
    }
}

export { Resource };
