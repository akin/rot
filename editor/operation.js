'use strict';

import {Element} from "./element.js";
import {Node} from "./node.js";
import {Socket} from "./socket.js";
import {Plug} from "./connection.js";

var minimumSize = 20;
var fieldHeight = 20;
var fieldWidth = 100;
var fieldPadding = 5;

class SlotBase extends Element
{
    constructor(data)
    {
        super(data);

        this.item = this.container.group();
        this.socket = new Socket({
            container: this.item,
            input: this.input,
            x: 10,
            y: 10,
            radius: 6
        });
    }

    set socket(value)
    {
        this._socket = value;
    }

    get socket()
    {
        return this._socket;
    }

    _reDraw()
    {
        this.socket._reDraw();
    }

    setSize(value)
    {
        if(this._body == null)
        {
            return;
        }

        if(value.x < minimumSize)
        {
            value.x = minimumSize;
        }

        this._body.width(value.x);
    }

    getSize()
    {
        if(this._body == null)
        {
            return super.getSize();
        }
        return new Vec2(this._body.width(), this._body.height());
    }
}

class InputSlot extends SlotBase
{
    constructor(data)
    {
        super(data);

        this._body = this.item.rect(10, fieldHeight);
        this._body.radius(3);
        this._body.addClass('field');
        this._body.addClass('base');

        // Need to limit text inside the rect somehow!
        this._text = this.item.text(data.key);
        this._text.addClass('field');
        this._text.addClass('text');
        this._text.move(20, 0);
    }
}

class OutputSlot extends SlotBase
{
    constructor(data)
    {
        super(data);

        this._body = this.item.rect(10, fieldHeight);
        this._body.radius(3);
        this._body.addClass('field');
        this._body.addClass('base');

        // Need to limit text inside the rect somehow!
        this._text = this.item.text(data.key);
        this._text.addClass('field');
        this._text.addClass('text');
        this._text.move(20, 0);
    }


    setSize(value)
    {
        super.setSize(value);

        var position = new Vec2(value.x - 10, value.y - 10);
        this.socket.position = position;
    }
}

class Operation extends Node
{
    constructor(data) 
    {
        super(data);
        this._inputSlots = [];
        this._outputSlots = [];

        this.minimumSize = new Vec2(250, 100);
        this.size = this.minimumSize;

        this._yOffset = 25;

        this.addInputSlot({
            key: "inputti"
        });
        this.addInputSlot({
            key: "satan"
        });

        this.addOutputSlot({
            key: "asdsad"
        });
        this.addOutputSlot({
            key: "666"
        });
    }

    addInputSlot(data)
    {
        var item = new InputSlot({
            editor: this._editor,
            container: this._item,
            input: this._input,
            key: data.key
        });

        var index = this._inputSlots.length;
        this._inputSlots.push(item);

        item.size = new Vec2(fieldWidth, fieldHeight);
        item.position = new Vec2(0, index * (fieldHeight + fieldPadding) + this._yOffset);
    }

    addOutputSlot(data)
    {
        var item = new OutputSlot({
            editor: this._editor,
            container: this._item,
            input: this._input,
            key: data.key
        });

        var index = this._outputSlots.length;
        var size = this.size;
        this._outputSlots.push(item);

        item.size = new Vec2(fieldWidth, fieldHeight);
        item.position = new Vec2(size.x - fieldWidth, index * (fieldHeight + fieldPadding) + this._yOffset);
    }

    removeSlot(data)
    {
    }

    setPosition(value)
    {
        super.setPosition(value);
        // update all child positions as well!
        for(var i = 0 ; i < this._outputSlots.length ; ++i)
        {
            this._outputSlots[i]._reDraw();
        }
        for(var i = 0 ; i < this._inputSlots.length ; ++i)
        {
            this._inputSlots[i]._reDraw();
        }
    }

    setSize(value)
    {
        super.setSize(value);

        if(this._outputSlots == null || this._inputSlots == null)
        {
            return;
        }

        // Positioning..
        for(var i = 0 ; i < this._outputSlots.length ; ++i)
        {
            var item = this._outputSlots[i];

            item.position = new Vec2(value.x - fieldWidth, i * (fieldHeight + fieldPadding) + this._yOffset);
        }

        // Need to probably move things according to this..
        for(var i = 0 ; i < this._outputSlots.length ; ++i)
        {
            this._outputSlots[i]._reDraw();
        }
        for(var i = 0 ; i < this._inputSlots.length ; ++i)
        {
            this._inputSlots[i]._reDraw();
        }
    }
}

export { Operation };