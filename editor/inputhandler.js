'use strict';

class InputHandler
{
    constructor(data) 
    {
        this._container = data.container;
        this._drag = new DragState();
        this._plug = new PlugState();
        var that = this;

        var onUpdate = function(event)
        {
            var item = {
                x: event.offsetX,
                y: event.offsetY,
            };
            if(that._drag.active())
            {
                that._drag.update(item);
            }
            if(that._plug.active())
            {
                that._plug.update(item);
            }
        };
        var onEnd = function(event)
        {
            if(that._drag.active())
            {
                that._drag.deactivate();
            }
            if(that._plug.active())
            {
                that._plug.deactivate();
            }
        };

        var isOnCanvas = function(current)
        {
            if(current == null)
            {
                return false;
            }
            if(current != that._container.node)
            {
                return isOnCanvas(current.parentNode);
            }
            return true;
        }

        this._container.on("mousemove", onUpdate);
        this._container.on("mouseup", onEnd);
        this._container.on("mouseout", function(event) {
            if(!isOnCanvas(event.toElement)) {
                onEnd(event);
            }
        });
    }

    startDrag(data)
    {
        this._drag.activate({
            position: data.item.position,
            offset: {
                x: data.event.offsetX,
                y: data.event.offsetY
            },
            onUpdate: data.onUpdate,
            onEnd: data.onEnd
        });
    }

    startPlug(data)
    {
        this._plug.activate({
            position: data.item.position,
            offset: {
                x: data.event.offsetX,
                y: data.event.offsetY
            },
            onUpdate: data.onUpdate,
            onEnd: data.onEnd
        });
    }

    addSocket(socket)
    {
        this._plug.add(socket);
    }

    removeSocket(socket)
    {
        this._plug.remove(socket);
    }
};

class DragState
{
    constructor()
    {
        this._active = false;

        this._position = new Vec2();
        this._current = new Vec2();
        this._delta = new Vec2();

        this._onupdate = null;
        this._onend = null;
    }

    get position()
    {
        return this._current;
    }

    getCurrent()
    {
        return {
            position: this._current,
            delta: this._current.subtract(this._position)
        };
    }

    activate(data)
    {
        this._active = true;

        this._position.set(data.position);
        this._current.set(this._position);

        this._delta.set(
            this._position.x - data.offset.x,
            this._position.y - data.offset.y
        );

        this._onupdate = data.onUpdate;
        this._onend = data.onEnd;
    }

    deactivate()
    {
        if(this._active)
        {
            if(this._onend != null)
            {
                this._onend(this.getCurrent());
            }

            this._active = false;
            this._onupdate = null;
            this._onend = null;
        }
    }

    update(offset)
    {
        if(this._active)
        {
            this._current.set(
                offset.x + this._delta.x,
                offset.y + this._delta.y
            );
        }
        if(this._onupdate != null)
        {
            this._onupdate(this.getCurrent());
        }
    }

    active()
    {
        return this._active;
    }
};

function findClosest(position, data)
{
    if(data.length == 0)
    {
        return null;
    }

    var found = data[0];
    var distance = position.distance(found.getWorldPosition());
    for(var i = 1 ; i < data.length  ; ++i)
    {
        var current =  position.distance(data[i].getWorldPosition());
        if(current < distance)
        {
            found = data[i];
            distance = current;
        }
    }

    return {
        item: found,
        distance: distance
    };
}

class PlugState extends DragState
{
    constructor()
    {
        super();
        this._sockets = [];
    }

    add(socket)
    {
        this._sockets.push(socket);
    }

    remove(socket)
    {
        for(var i = this._sockets.length - 1 ; i > 0  ; --i)
        {
            if(this._sockets[i] == socket)
            {
                this._sockets[i].splice(i, 1);
            }
        }
    }

    getCurrent()
    {
        var socket = findClosest(this.position, this._sockets);

        if(socket == null)
        {
            return {
                position: this._current,
                found: false
            };
        }

        return {
            position: this._current,
            found: true,
            socket: socket.item,
            distance: socket.distance,
        };
    }
};

export { InputHandler };