'use strict';
import {Element} from "./element.js";

class Item extends Element
{
    constructor(data) 
    {
        super(data);
        // Make group..
        // Contains, box
        // lines of text,
        // 2 sides
        // input
        // output 
        // Maybe description & name
        // drag & drop lines to input & outputs

        this.item = this.container.rect(200, 100);
        this.item.move(data.x, data.y);
        this.item.addClass('item');
        this.item.radius(10);

        var that = this;
        this.item.on("mousedown", function(event)
        {
            that.activate();
            that.input.startDrag({
                item: that,
                event: event,
                onUpdate(data) {
                    that.position = data.position;
                }
            });
        });
    }
}

class CreateItem
{
    constructor(data) 
    {
    }
}

export { Item, CreateItem };