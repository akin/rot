'use strict';

class Element
{
    constructor(data)
    {
        this._editor = data.editor;
        this._container = data.container;
        this._input = data.input;
        this._item = null;
        this._name = "";
    }

    setPosition(value)
    {
        if(this._item != null)
        {
            this._item.x(value.x);
            this._item.y(value.y);
        }
    }

    getPosition()
    {
        if(this._item == null)
        {
            return new Vec2(0, 0);
        }
        return new Vec2(this._item.x(), this._item.y());
    }

    getWorldPosition()
    {
        var vec = this.position;
        var parents = this.item.parents();
        parents.pop(); // remove the last, as it is the world space we are in.

        for(var i = 0; i < parents.length ; ++i)
        {
            var parent = parents[i];
            vec.x += parent.x();
            vec.y += parent.y();
        }

        return vec;
    }

    set position(value)
    {
        this.setPosition(value);
    }

    get position()
    {
        return this.getPosition();
    }

    setSize(value)
    {
    }

    getSize()
    {
        return new Vec2(0, 0);
    }

    set size(value)
    {
        this.setSize(value);
    }

    get size()
    {
        return this.getSize();
    }

    set name(value)
    {
        this._name = value;
    }

    get name()
    {
        return this._name;
    }

    set item(value)
    {
        var old = this._item;
        this._item = value;
    }

    get item()
    {
        return this._item;
    }

    get draggable()
    {
        return false;
    }

    set container(value)
    {
        this._container = value;
    }

    get container()
    {
        return this._container;
    }

    get input()
    {
        return this._input;
    }

    get editor()
    {
        return this._editor;
    }

    set onUpdate(value)
    {
        this._onUpdate= value;
    }

    get onUpdate()
    {
        if(this._onUpdate == null)
        {
            return function(){};
        }
        return this._onUpdate;
    }

    toFront()
    {
        if(this._item != null)
        {
            this._item.front();
        }
    }

    activate()
    {
        this.toFront();
    }

    serialize(data)
    {
    }

    deserialize(data)
    {
    }
}

export { Element };