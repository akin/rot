'use strict';
import {Item, CreateItem} from "./item.js";
import {Operation} from "./operation.js";
import {Connection} from "./connection.js";
import {InputHandler} from "./inputhandler.js";
import {Node} from "./node.js";
//import {World} from "./world/world.js";

class Editor
{
    constructor(data) 
    {
        this._element = data.element;
        this._data = {};
        this._container = SVG(this._element);
        this._input = new InputHandler({
            container: this._container
        });

        var item2 = new Connection({
            editor: this,
            container: this._container,
            input: this._input,
            p: {x: 50, y:20},
            p2: {x: 140, y: 40}
        });


        for(var i = 0 ; i < 10 ; ++i)
        {
            var item = new Connection({
                editor: this,
                container: this._container,
                input: this._input,
                p: {x: Math.random() * 500, y: Math.random() * 500},
                p2: {x: Math.random() * 500, y: Math.random() * 500}
            });
        }

        for(var i = 0 ; i < 5 ; ++i)
        {
            var item = new Operation({
                editor: this,
                container: this._container,
                input: this._input,
                x: Math.random() * 500,
                y: Math.random() * 500
            });
        }
    }

    serialize(data)
    {
    }

    deserialize(data)
    {
    }

    transform(data)
    {
        this._container.transform(data);
    }
}

export { Editor };