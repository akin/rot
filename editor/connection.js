'use strict';

import {Element} from "./element.js";

/**
* Positioning!
* Groups and grouping mechanisms are nice, Except, when you need to reference between those dimensions..
* SO! to make thins manageable, Connections!, work in worldspace, canvas space, on the lowest level of space,
* they should not be grouped, and if you assing value to Plug position or link position, make sure, it is in worldspace.
*/
class Plug extends Element
{
    constructor(data)
    {
        super(data);
        this.radius = data.radius;

        this.attachDistance = 20;
        this._connection = data.connection;
        this.item = this.container.circle(this.radius * 2);
        this.item.addClass('point');
        this.item.move(
            data.x - this.radius,
            data.y - this.radius
        );

        var that = this;
        this.item.on("mousedown", function(event)
        {
            that.activate();
            that.input.startPlug({
                item: that,
                event: event,
                onUpdate(data) {
                    if(!data.found)
                    {
                        that.position = data.position;
                        return;
                    }

                    if(data.distance < that.attachDistance)
                    {
                        that.position = data.socket.getWorldPosition();
                    }
                    else
                    {
                        that.position = data.position;
                    }
                },
                onEnd(data) {
                    if(!data.found)
                    {
                        that.position = data.position;
                        return;
                    }

                    if(data.distance < that.attachDistance)
                    {
                        that.socket = data.socket;
                    }
                }
            });
        });

        // Attach socket, if there is one.
        this.socket = data.socket;
    }

    set position(value)
    {
        this.item.x(value.x - this.radius);
        this.item.y(value.y - this.radius);

        this.connection._reDraw();
    }

    get position()
    {
        return new Vec2(
            this.item.x() + this.radius,
            this.item.y() + this.radius
        );
    }

    get radius()
    {
        return this._radius;
    }

    set radius(val)
    {
        this._radius = val;
    }

    get socket()
    {
        return this._socket;
    }

    set socket(val)
    {
        if(this._socket != null)
        {
            this._socket.removePlug(this);
        }
        this._socket = val;
        if(this._socket != null)
        {
            this._socket.addPlug(this);
            this.position = this._socket.getWorldPosition();
        }
        this.onUpdate();
        this.connection.onUpdate();
    }

    get connection()
    {
        return this._connection;
    }
}

class Connection extends Element
{
    constructor(data)
    {
        super(data);

        var p1 = data.p;
        var p2 = data.p2;

        this.source = new Plug({
            container: this.container,
            input: this.input,
            connection: this,
            socket: null,
            x: p1.x,
            y: p1.y,
            radius: 5
        });

        this.target = new Plug({
            container: this.container,
            input: this.input,
            connection: this,
            socket: null,
            x: p2.x,
            y: p2.y,
            radius: 5
        });

        this.item = this.container.line(0, 0, 0, 0);
        this.item.stroke({ width: 1 });
        this.item.addClass('line');

        this._reDraw();
    }

    _reDraw()
    {
        var a = this.source.position;
        var b = this.target.position;

        this.item.plot(
            a.x,
            a.y,
            b.x,
            b.y);

        this.toFront();
    }

    get source()
    {
        return this._source;
    }

    set source(value)
    {
        this._source = value;
    }

    get target()
    {
        return this._target;
    }

    set target(value)
    {
        this._target = value;
    }

    toFront()
    {
        super.toFront();
        this.source.toFront();
        this.target.toFront();
    }
}

export { Connection , Plug };
