'use strict';

import {Element} from "./element.js";

var resizeTagSize = 20;
var minimumSize = 50;

class Node extends Element
{
    constructor(data)
    {
        super(data);

        this._minimum = new Vec2(minimumSize, minimumSize);
        this.item = this.container.group();
        this.item.move(data.x, data.y);

        this.createItems();

        if(data.width != null && data.height != null)
        {
            this.size = new Vec2(data.width, data.height);
        }
        else
        {
            this.size = new Vec2(200, 100);
        }

        var that = this;
        this._head.on("mousedown", function(event)
        {
            that.activate();
            that.input.startDrag({
                item: that,
                event: event,
                onUpdate(data) {
                    that.position = data.position;
                }
            });
        });
        this._resize.on("mousedown", function(event)
        {
            var size = new Vec2(that.size);
            that.activate();
            that.input.startDrag({
                item: that,
                event: event,
                onUpdate(data) {
                    that.size = data.delta.add(size);
                }
            });
        });
    }

    createItems()
    {
        this._body = this.item.rect(1, 1);
        this._body.radius(5);
        this._body.addClass('node');
        this._body.addClass('body');

        this._head = this.item.rect(1, 1);
        this._head.radius(5);
        this._head.addClass('node');
        this._head.addClass('head');

        this._resize = this.item.group();

        this._resize_tag = this._resize.rect(resizeTagSize, resizeTagSize);
        this._resize_tag.radius(5);
        this._resize_tag.addClass('node');
        this._resize_tag.addClass('resize');

        this._resize_line = this._resize.line().addClass('node').addClass('resize').addClass('stroke');
        this._resize_line2 = this._resize.line().addClass('node').addClass('resize').addClass('stroke');
        this._resize_line3 = this._resize.line().addClass('node').addClass('resize').addClass('stroke');

        var offset = resizeTagSize * 0.3;
        var one = resizeTagSize * 0.9;
        var half = resizeTagSize * 0.5;
        var quarter = resizeTagSize * 0.25;

        this._resize_line.plot(offset, one, one, offset);
        this._resize_line2.plot(offset + half, one, one, offset + half);
        this._resize_line3.plot(offset + quarter, one, one, offset + quarter);
    }

    setPosition(value)
    {
        super.setPosition(value);
        // update all child positions as well!
    }

    set minimumSize(value)
    {
        this._minimum = value;
    }

    get minimumSize()
    {
        return this._minimum;
    }

    setSize(value)
    {
        if(value.x < this._minimum.x)
        {
            value.x = this._minimum.x;
        }
        if(value.y < this._minimum.y)
        {
            value.y = this._minimum.y;
        }

        this._body.size(value.x, value.y);
        this._head.size(value.x, 20);
        this._resize.move(value.x - resizeTagSize, value.y - resizeTagSize);
    }

    getSize()
    {
        return new Vec2(this._body.width(), this._body.height());
    }
}

export { Node };