
'use strict';

import {Editor} from "./editor.js";

// Bootstrap it
$(function(){
    $("[editor]").each(function(){
        // should have "editor" attribute..
        (function(thiz){
            var url = thiz.getAttribute("editor");
            var self = $(thiz);
            $.getJSON(url, function( data ) {
                // Load & populate self object..
                window.editor = new Editor({
                    element: thiz
                });
            });
        })(this);
    });
});