'use strict';

import {Element} from "./element.js";
import {Plug} from "./connection.js";

class Socket extends Element
{
    constructor(data)
    {
        super(data);
        this.radius = data.radius;

        this._plug = [];
        this.item = this.container.circle(this.radius * 2);
        this.item.stroke({ width: 2 });
        this.item.addClass('point');

        this.item.move(
            data.x - this.radius,
            data.y - this.radius
        );

        this.input.addSocket(this);

        this._reDraw();
    }

    _reDraw()
    {
        for(var i = this._plug.length - 1 ; i >= 0 ; --i)
        {
            this._plug[i].position = this.getWorldPosition();
        }
    }

    toFront()
    {
        super.toFront();
        this.source.toFront();
        this.target.toFront();
    }

    addPlug(plug)
    {
        this._plug.push(plug);
        this.onUpdate();
    }

    removePlug(plug)
    {
        for(var i = this._plug.length - 1 ; i >= 0 ; --i)
        {
            if(this._plug[i] == plug)
            {
                this._plug.splice(i, 1);
            }
        }
        this.onUpdate();
    }

    set position(value)
    {
        this.item.x(value.x - this.radius);
        this.item.y(value.y - this.radius);

        this._reDraw();
    }

    get position()
    {
        return new Vec2(
            this.item.x() + this.radius,
            this.item.y() + this.radius
        );
    }

    get radius()
    {
        return this._radius;
    }

    set radius(val)
    {
        this._radius = val;
    }
}

export { Socket };
