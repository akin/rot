
class Vec2
{
    constructor(a,b)
    {
        this._x = 0;
        this._y = 0;

        this.set(a,b);
    }

    set(a,b)
    {
        if(b != undefined)
        {
            this._x = parseFloat(a);
            this._y = parseFloat(b);
        }
        else if(a != undefined)
        {
            if(isNaN(a))
            {
                if(a.x != undefined)
                {
                    this._x = parseFloat(a.x);
                }
                if(a.y != undefined)
                {
                    this._y = parseFloat(a.y);
                }
            }
            else
            {
                this._x = parseFloat(x);
            }
        }
    }

    get x()
    {
        return this._x;
    }

    set x(value)
    {
        this._x = parseFloat(value);
    }

    get y()
    {
        return this._y;
    }

    set y(value)
    {
        this._y = parseFloat(value);
    }

    distance(second)
    {
        var x = this._x - second.x;
        var y = this._y - second.y;
        return Math.sqrt( x*x + y*y );
    }

    subtract(other)
    {
        return new Vec2(this._x - other._x, this._y - other._y);
    }

    add(other)
    {
        return new Vec2(this._x + other._x, this._y + other._y);
    }

    clone()
    {
        return new Vec2(this._x, this._y);
    }
}
