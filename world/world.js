'use strict';

class World
{
    constructor() 
    {
        this._data = {};
    }

    hasType(type)
    {
        return true;
    }

    add(typestr, item)
    {
        var type = typestr.toLowerCase();
        switch(type)
        {
            case "resource" : 
            {
                
            }
        }

        var container = this._data[type];
        if(container == undefined)
        {
            container = [];
            this._data[type] = container;
        }
        container.push(item);
    }
}

export { World };